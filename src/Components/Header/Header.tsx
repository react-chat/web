import * as React from 'react';
import './Header.css';

class Header extends React.Component {
    public render() {
        return (
            <div className="header">
                <div className="header-text">React chat</div>
            </div>
        )
    }   
}

export default Header;