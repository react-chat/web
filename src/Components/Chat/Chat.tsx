import * as React from 'react';
import './Chat.css'

import ChatLine, {IChatLineProps} from '../ChatLine/ChatLine';

// Libs
import * as openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:8000');

export interface IChatHistoryState {
    lines: IChatLineProps[];
    username: string;
    currentMessage: string;
}

class ChatHistory extends React.Component<{}, IChatHistoryState> {
    public state = {
        lines: [],
        username: "Default",
        currentMessage: ""
    };

    public messagesEnd: HTMLDivElement | null;

    public componentDidMount() {
        socket.on('connection', () => {
            // Force update to enable the input form
            this.forceUpdate();
        });

        socket.on("disconnect", () => {
            // Force update to disable the input form
            this.forceUpdate();
        });

        socket.on('usernameAssigned', (username: any) => {
            this.setState({"username": username});
        });

        socket.on("initHistory", (history: any) => {
            this.setState({lines: history});
        })

        socket.on("userConnected", (username: string) => {
            this.newLine({username: "", message: username + " connected", "timestamp": new Date().toISOString()});
        });

        socket.on("userDisconnected", (username: string) => {
            this.newLine({username: "", message: username + " disconnected", "timestamp": new Date().toISOString()});
        });

        socket.on("message", (data: any) => {
            this.newLine({username: data.username, message: data.message, "timestamp": data.timestamp});
            this.scrollToBottom();
        });

        this.scrollToBottom();

        // Update the timestamp frequently
        // setInterval(() => {
        //     this.setState({lines: this.state.lines});
        // }, 1000);
    }

    public componentDidUpdate() {
        this.scrollToBottom();
    }

    public render() {
        return (
            <div>
                {this.state.lines.map((line: IChatLineProps, index) => (
                    <ChatLine key={index} username={line.username} message={line.message} timestamp={line.timestamp}/>
                ))}

                <form onChange={this.handleChange} onSubmit={this.handleSubmit}>
                    <input disabled={socket.disconnected} name="currentMessage" className="form-control form-control-sm currentMessage" type="text" placeholder="Type message..." value={this.state.currentMessage} autoFocus={true} autoComplete="off"/>
                </form>

                <div style={{ float:"left", clear: "both" }} ref={(el) => { this.messagesEnd = el; }}/>
            </div>
        )
    }

    public scrollToBottom = () => {
        if (this.messagesEnd instanceof HTMLDivElement) {
            this.messagesEnd.scrollIntoView({ behavior: "instant" });
        }
    }

    public newLine = (item: IChatLineProps): any => {
        this.setState(prevState => (
            {lines: [...prevState.lines, item]}
        ));
    }
    
    private handleChange = (e: React.ChangeEvent<HTMLFormElement>) => {
        this.setState({"currentMessage": e.target.value});
    }

    private handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (socket.disconnected || this.state.currentMessage === "") {
            return;
        }
        const timestamp = new Date();
        socket.emit("message", {username: this.state.username, message: this.state.currentMessage}, (success: boolean) => {
            this.newLine({username: this.state.username, message: this.state.currentMessage, "timestamp": timestamp.toISOString()});
            this.setState({"currentMessage": ""});
            this.scrollToBottom();
        });      
    }
}


export default ChatHistory;