import * as React from 'react';
import './ChatLine.css'

export interface IChatLineProps {
    username: string;
    message: string;
    timestamp: string;
}

const ChatLine = (props: IChatLineProps) => (
	<div className="list-group">
		<div className="list-group-item list-group-item-action flex-column align-items-start">
			<div className="d-flex w-100 justify-content-between">
				<div className="username">{props.username}</div>
				<div className="timestamp">{timeSince(props.timestamp) + " ago"}</div>
			</div>
			<div className="message">{props.message}</div>
		</div>
	</div>
)

const timeSince = (dateRec: string) => {
	const currentDate: any = new Date().valueOf();
	const date = new Date(dateRec).valueOf();

	const seconds = Math.floor((currentDate - date) / 1000);
	let interval = Math.floor(seconds / 31536000);
	if (interval > 1) {
		return interval + " years";
	}
	interval = Math.floor(seconds / 2592000);
	if (interval > 1) {
		return interval + " months";
	}
	interval = Math.floor(seconds / 86400);
	if (interval > 1) {
		return interval + " days";
	}
	interval = Math.floor(seconds / 3600);
	if (interval > 1) {
		return interval + " hours";
	}
	interval = Math.floor(seconds / 60);
	if (interval > 1) {
		return interval + " minutes";
	}
	return Math.floor(seconds) + " seconds";
}

export default ChatLine;