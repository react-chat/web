import * as React from 'react';
import './Index.css';

// Components
import Header from '../../Components/Header/Header';
import Chat from '../../Components/Chat/Chat';
import NamePicker from '../../Components/NamePicker/NamePicker';

const Index = () => (
  <div >
    <Header/>
    <Chat/>
    <NamePicker/>
  </div>
);

export default Index;